function tinhTienDien() {
  var tenKH = document.getElementById("tenKH").value;
  var soKw = parseInt(document.getElementById("soKw").value);
  var tien = 0;

  if (soKw <= 50) {
    tien = soKw * 500;
  } else if (soKw <= 100) {
    tien = 50 * 500 + (soKw - 50) * 650;
  } else if (soKw <= 150) {
    tien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
  } else {
    tien = 50 * 500 + 50 * 650 + 50 * 850 + (soKw - 150) * 1100;
  }

  tien = tien.toLocaleString();
  document.getElementById("kqTD").innerHTML = `Khách hàng: ${tenKH}<br>
    Tiền điện: ${tien} VNĐ`;
  document.getElementById("kqTD").style.display = "block";
}

// Trong đó, chúng ta sử dụng một form HTML để nhập thông tin về tên và số KW tiêu thụ điện của người dùng. Sau đó, khi người dùng bấm nút "Tính", chúng ta sẽ gọi hàm JavaScript tinhTienDien() để tính toán và xuất kết quả trên trang web.

// Trong hàm tinhTienDien(), chúng ta sử dụng câu lệnh document.getElementById() để lấy giá trị của các trường nhập liệu từ form HTML. Sau đó, chúng ta tính toán tiền điện bằng cách kiểm tra số KW tiêu thụ và áp dụng các quy tắc giá tiền như yêu cầu.

// Cuối cùng, chúng ta sử dụng câu lệnh document.getElementById() và thuộc tính innerHTML để xuất kết quả tính toán lên trang web.
