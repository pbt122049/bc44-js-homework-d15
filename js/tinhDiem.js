function tinhDiem() {
  // Lấy các giá trị nhập liệu
  var diemChuan = parseFloat(document.getElementById("diem-chuan").value);
  var diemMon1 = parseFloat(document.getElementById("diem-mon-1").value);
  var diemMon2 = parseFloat(document.getElementById("diem-mon-2").value);
  var diemMon3 = parseFloat(document.getElementById("diem-mon-3").value);
  var khuVuc = document.getElementById("khu-vuc").value;
  var doiTuong = document.getElementById("doi-tuong").value;

  // Tính tổng điểm
  var tongDiem = diemMon1 + diemMon2 + diemMon3;

  // Tính điểm ưu tiên
  // Khai báo biến điểm ưu tiên ban đầu bằng 0
  var diemUuTien = 0;

  // Kiểm tra điểm ưu tiên theo khu vực
  if (khuVuc === "A") {
    diemUuTien += 2;
  } else if (khuVuc === "B") {
    diemUuTien += 1;
  } else if (khuVuc === "C") {
    diemUuTien += 0.5;
  }

  // Kiểm tra điểm ưu tiên theo đối tượng
  if (doiTuong === "1") {
    diemUuTien += 2.5;
  } else if (doiTuong === "2") {
    diemUuTien += 1.5;
  } else if (doiTuong === "3") {
    diemUuTien += 1;
  }

  // Tính tổng điểm với điểm ưu tiên
  tongDiem += diemUuTien;

  // Kiểm tra kết quả
  if (tongDiem >= diemChuan && diemMon1 > 0 && diemMon2 > 0 && diemMon3 > 0) {
    // Thí sinh đậu
    document.getElementById("kqTS").innerHTML =
      "Đậu với tổng số điểm " + tongDiem;
  } else {
    // Thí sinh rớt
    document.getElementById("kqTS").innerHTML = "Rớt";
    document.getElementById("kqTS").style.display = "block";
  }
}

// Trong hàm `tinhDiem()`, bạn lấy các giá trị nhập liệu từ các trường nhập liệu, tính tổng điểm của 3 môn thi và tính điểm ưu tiên dựa trên khu vực và đối tượng. 
// Sau đó, bạn tính tổng điểm với điểm ưu tiên và kiểm tra kết quả. 
// Nếu tổng điểm lớn hơn hoặc bằng điểm chuẩn và không có môn nào điểm 0, thì thí sinh đậu. Ngược lại, thí sinh rớt.
