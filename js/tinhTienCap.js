function toggleInputs() {
  var customerTypeSelect = document.getElementById("customer-type");
  var connectionInputsDiv = document.getElementById("connection-inputs");
  if (customerTypeSelect.value === "doanh-nghiep") {
    connectionInputsDiv.style.display = "block";
  } else {
    connectionInputsDiv.style.display = "none";
  }
}

function calculateBill() {
  var customerTypeSelect = document.getElementById("customer-type");
  var numberOfConnectionsInput = document.getElementById(
    "number-of-connections"
  );
  var numberOfChannelsInput = document.getElementById("number-of-channels");
  var bill = document.getElementById("bill");

  var processingFee = 0;
  var basicServiceFee = 0;
  var premiumChannelFee = 0;
  var totalFee = 0;

  if (customerTypeSelect.value === "doanh-nghiep") {
    processingFee = 15;
    basicServiceFee = 75;
    premiumChannelFee = 50 * numberOfChannelsInput.value;
    totalFee =
      processingFee +
      basicServiceFee +
      premiumChannelFee +
      5 * numberOfConnectionsInput.value;
  } else {
    processingFee = 4.5;
    basicServiceFee = 20.59;
    premiumChannelFee = 7.5 * numberOfChannelsInput.value;
    totalFee = processingFee + basicServiceFee + premiumChannelFee;
  }

  bill.innerHTML = `<p>Phí xử lý hóa đơn: ${processingFee.toFixed(2)}$ </p>
    <p>Phí dịch vụ cơ bản: ${basicServiceFee.toFixed(2)}$ </p>
    <p>Phí thuê kênh cao cấp: ${premiumChannelFee.toFixed(2)}$ </p>
    <p>Thành tiền: ${totalFee.toFixed(2)}$</p>`;
  bill.style.display = "block";
}

// Mã trên bao gồm một biểu mẫu HTML cho phép người dùng nhập thông tin khách hàng và một script JavaScript để tính toán hóa đơn.

// Biểu mẫu bao gồm các trường nhập liệu cho mã khách hàng, loại khách hàng, số kết nối và số kênh cao cấp. Trường nhập liệu số kết nối được hiển thị nếu loại khách hàng được chọn là Doanh nghiệp và ẩn đi nếu loại khách hàng được chọn là Nhà dân. Khi người dùng nhấn nút "Tính hóa đơn", script JavaScript sẽ tính toán các phí và hiển thị kết quả trong một div có id là "bill".

// Script JavaScript bắt đầu bằng hai hàm: toggleInputs và calculateBill. Hàm toggleInputs được gọi khi người dùng thay đổi loại khách hàng. Nếu loại khách hàng được chọn là Doanh nghiệp, trường nhập liệu số kết nối sẽ được hiển thị; nếu loại khách hàng được chọn là Nhà dân, trường nhập liệu số kết nối sẽ được ẩn đi.

// Hàm calculateBill được gọi khi người dùng nhấn nút "Tính hóa đơn". Nó lấy các giá trị đầu vào từ các trường nhập liệu và tính toán các phí theo các quy tắc được định nghĩa cho Nhà dân và Doanh nghiệp. Sau đó, nó hiển thị kết quả trong div "bill".

// Chương trình này có thể được tùy chỉnh và mở rộng để thêm các tính năng khác, chẳng hạn như tính toán thuế hoặc hiển thị các chi tiết hóa đơn khác nhau cho các loại khách hàng khác nhau.
