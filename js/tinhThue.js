function calculateTax() {
    const name = document.getElementById("name").value;
    const income = Number(document.getElementById("income").value);
    const dependents = Number(document.getElementById("dependents").value);

    const taxableIncome = income - 4e+6 - dependents * 1.6e+6;

    let tax = 0;
    if (taxableIncome <= 6e+7) {
      tax = taxableIncome * 0.05;
    } else if (taxableIncome <= 1.2e+8) {
      tax = 3e+6 + (taxableIncome - 6e+7) * 0.1;
    } else if (taxableIncome <= 2.1e+8) {
      tax = 9e+6 + (taxableIncome - 1.2e+8) * 0.15;
    } else if (taxableIncome <= 3.84e+8) {
      tax = 18e+6 + (taxableIncome - 2.1e+8) * 0.2;
    } else if (taxableIncome <= 6.24e+8) {
      tax = 66e+6 + (taxableIncome - 3.84e+8) * 0.25;
    } else if (taxableIncome <= 9.6e+8) {
      tax = 141e+6 + (taxableIncome - 6.24e+8) * 0.3;
    } else {
      tax = 246e+6 + (taxableIncome - 9.6e+8) * 0.35;
    }

    const result = document.getElementById("kqTT");
    result.innerHTML = `${name} phải nộp thuế thu nhập cá nhân là: <strong>${tax.toLocaleString()} VNĐ</strong>`;
    result.style.display = "block";
  }

//   Trong đoạn mã này, chúng ta sử dụng một biểu mẫu HTML để nhập thông tin của cá nhân, bao gồm họ tên, tổng thu nhập năm và số người phụ thuộc. 
//   Khi người dùng nhấp vào nút "Tính thuế", chúng ta sẽ lấy giá trị của các trường đầu vào và tính toán thuế thu nhập theo quy định đã cho. 
//   Kết quả sẽ được hiển thị trong một phần tử p có id kqTT.  